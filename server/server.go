package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/url"
	"os"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/cors"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/tidwall/shardmap"

	sq "github.com/elgris/sqrl"
	sqlbuilder "github.com/huandu/go-sqlbuilder"

	"github.com/gofiber/fiber"
	// "github.com/gofiber/logger"
	"gitlab.com/silasb/superglue/logger"
	"gitlab.com/silasb/superglue/tokens"

	// "server/structor"

	"github.com/gofiber/recover"

	"github.com/go-playground/form/v4"

	jwtware "github.com/gofiber/jwt" // jwtware
)

var decoder *form.Decoder
var cache StructCache
var jwtBlacklist *shardmap.Map

// var m = &sync.Mutex{} // even as global var

func Start() {
	decoder = form.NewDecoder()
	cache = NewStructCache()

	// jwtBlacklist = make(map[string]bool, 0)
	jwtBlacklist = shardmap.New(100)

	app := fiber.New()
	app.Use(cors.New())
	app.Use(logger.New(logger.Config{
		Format: "${time} ${method} ${path} - ${ip} - ${status} - ${latency}\nParams ${query}\n",
	}))

	app.Use(recover.New(recover.Config{
		Handler: func(c *fiber.Ctx, err error) {
			c.SendString(err.Error())
			c.SendStatus(500)
		},
	}))

	app.Post("/token", token)

	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte("secret"),
	}))

	app.Use(func(c *fiber.Ctx) {
		token := c.Locals("user").(*jwt.Token)
		claims := token.Claims.(jwt.MapClaims)
		otp, ok := claims["otp"].(bool)
		if !ok {
			otp = false
		}

		if _, ok := jwtBlacklist.Get(token.Raw); otp && ok {
			c.Status(fiber.StatusUnauthorized)
			c.SendString("Invalid or expired JWT")
		} else {
			c.Next()
			// after we are finished with the request blacklist the JWT
			jwtBlacklist.Set(token.Raw, true)
		}
	})

	app.Post("/csv", createTable)
	app.Post("/:db/insert", insert)
	app.Get("/:db/query", query)
	app.Post("/:db/update", update)

	app.Listen("0.0.0.0:3000")
}

func token(c *fiber.Ctx) {
	token, err := tokens.Generate(nil, &map[string]interface{}{
		"actions[*]": []string{
			"query", "update",
		},
	})
	if err != nil {
		c.SendStatus(fiber.StatusInternalServerError)
		return
	}

	c.JSON(fiber.Map{"token": token})
}

func createTable(c *fiber.Ctx) {
	name := c.FormValue("name")
	file, err := c.FormFile("db")

	if err == nil {
		// Save file to root directory:
		fmpFile := fmt.Sprintf("/tmp/%s", file.Filename)
		c.SaveFile(file, fmpFile)

		dir, err := os.Getwd()
		if err != nil {
			log.Panic(err)
		}

		f, _ := os.Open(file.Filename)
		headers := CSVToHeader(f)
		log.Println("headers:", headers)
		createTable := CreateTableTemplate(name, headers)
		log.Println(createTable)
		db, err := Connectx(fmt.Sprintf("%s/%s.db", dir, name))
		if err != nil {
			log.Panic(err)
		}
		db.MustExec(createTable)
		createTriggers := CreateTriggers(name)
		db.MustExec(createTriggers)

		f.Seek(0, 0)
		csv := CSVToMap(f)
		for _, rows := range csv {
			q := sq.Insert(name)
			var values []interface{}

			for key, value := range rows {
				fmt.Println(key, value)
				q.Columns(key)

				values = append(values, value)
			}
			q.Values(values...)
			sql, args, _ := q.ToSql()
			log.Println(sql, args)
			db.MustExec(sql, args...)
		}
	} else {
		log.Panic(err)
	}

	c.Send("File imported")
}

func insert(c *fiber.Ctx) {
	dbName := c.Params("db")
	if ok, err := isAuthorized("insert", dbName, c.Locals("user")); !ok {
		c.Status(fiber.StatusUnauthorized)
		c.SendString(err.Error())
		return
	}

	dbFile := GetDB(dbName)

	insertValues := make(map[string]interface{})
	err := json.Unmarshal([]byte(c.Body()), &insertValues)

	res, err := Insert(dbFile, dbName, insertValues)
	if err != nil {
		c.Status(500).Send(err)
		return
	}

	id, _ := res.LastInsertId()
	query := JSONAPIFilter{
		"id": []string{strconv.FormatInt(id, 10)},
	}
	results, err := QueryAPIResonse(dbFile, dbName, query, nil)
	if err != nil {
		c.Status(500).Send(err)
		return
	}

	WriteJSONApiResponse(c, results)
}

func query(c *fiber.Ctx) {
	dbName := c.Params("db")
	if ok, err := isAuthorized("query", dbName, c.Locals("user")); !ok {
		c.Status(fiber.StatusUnauthorized)
		c.SendString(err.Error())
		return
	}

	dbFile := GetDB(dbName)

	params, _ := url.ParseQuery(c.Fasthttp.QueryArgs().String())

	jsonapi := JSONAPI{}
	err := decoder.Decode(&jsonapi, params)
	if err != nil {
		log.Panic(err)
	}

	results, err := QueryAPIResonse(dbFile, dbName, jsonapi.Filter, jsonapi.Field)
	if err != nil {
		c.Status(500).Send(err)
		return
	}

	WriteJSONApiResponse(c, results)
}

func update(c *fiber.Ctx) {
	dbName := c.Params("db")
	if ok, err := isAuthorized("update", dbName, c.Locals("user")); !ok {
		c.Status(fiber.StatusUnauthorized)
		c.SendString(err.Error())
		return
	}

	dbFile := GetDB(dbName)

	params, _ := url.ParseQuery(c.Fasthttp.QueryArgs().String())

	jsonapi := JSONAPI{}
	err := decoder.Decode(&jsonapi, params)
	if err != nil {
		log.Panic(err)
	}

	updatedValues := make(map[string]interface{})
	json.Unmarshal([]byte(c.Body()), &updatedValues)

	Update(dbFile, dbName, jsonapi.Filter, updatedValues)

	results, err := QueryAPIResonse(dbFile, dbName, jsonapi.Filter, jsonapi.Field)
	if err != nil {
		c.Status(500).Send(err)
		return
	}

	WriteJSONApiResponse(c, results)
}

func CreateTableTemplate(name string, headers []string) string {
	ctb := sqlbuilder.NewCreateTableBuilder()
	ctb.CreateTable(name).IfNotExists()
	// uid blob primary key CHECK(typeof(uid)='blob' and length(uid)=16)
	// randomblob(16)
	// ctb.Define("id", "BLOB", "PRIMARY KEY")
	ctb.Define("id", "INTEGER", "PRIMARY KEY")
	for _, header := range headers {
		ctb.Define(header, "TEXT")
	}
	ctb.Define("_created_at", "TIMESTAMP", "NOT NULL", "DEFAULT (datetime('now', 'localtime'))")
	ctb.Define("_updated_at", "DATETIME")
	// ctb.Define("KEY", "idx_name_modified_at", "name, modified_at")

	return ctb.String()
}

func CreateTriggers(dbName string) string {
	return fmt.Sprintf(`
	CREATE TRIGGER update_trigger AFTER UPDATE ON %s
	BEGIN
		update %s SET _updated_at = datetime('now') WHERE rowid = NEW.rowid;
	END;
	`, dbName, dbName)
}

func RowsToJSONApiResponse(dbFile, dbName string, rows *sqlx.Rows, results *JSONAPIResponse) {
	dynamicStruct := GetStruct(dbFile, dbName)
	UnmarshalStruct(rows, results, dynamicStruct)
}

func QueryAPIResonse(dbFile, dbName string, values JSONAPIFilter, fields JSONAPIField) (*JSONAPIResponse, error) {
	rows, err := Query(dbFile, dbName, values, fields)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results JSONAPIResponse
	RowsToJSONApiResponse(dbFile, dbName, rows, &results)

	return &results, nil
}

func isAuthorized(action, dbName string, user interface{}) (bool, error) {
	token := user.(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	actions, ok := claims[fmt.Sprintf("actions[%s]", dbName)].([]interface{})

	if !ok {
		actions, ok = claims["actions[*]"].([]interface{})
		if !ok {
			return false, errors.New("Trying to access a database your JWT is not authorized to access")
		}
	}

	b := make([]string, len(actions))

	for i, v := range actions {
		switch typedValue := v.(type) {
		case string:
			b[i] = typedValue
		}
	}

	for _, jwtAction := range b {
		if action == jwtAction {
			return true, nil
		}
	}

	return false, errors.New("Cannot find any actions that your JWT is allowed to do")
}
