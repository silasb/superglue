package server

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"

	sq "github.com/elgris/sqrl"
	"github.com/jmoiron/sqlx"

	"github.com/pkg/errors"
)

// Database instance
// var DB *sql.DB

// Connect function
func Connect(db_path string) (*sql.DB, error) {
	// var err error
	db, err := sql.Open("sqlite3", db_path)

	if err != nil {
		log.Panic(err)
	}

	if err = db.Ping(); err != nil {
		return db, err
	}
	return db, nil
}

// Connect function
func Connectx(db_path string) (*sqlx.DB, error) {
	// var err error
	db, err := sqlx.Open("sqlite3", db_path)

	if err != nil {
		log.Panic(err)
	}

	if err = db.Ping(); err != nil {
		return db, err
	}
	return db, nil
}

func GetDB(dbName string) string {
	dir, err := os.Getwd()
	if err != nil {
		log.Panic(err)
	}

	dbFile := fmt.Sprintf("%s/%s.db", dir, dbName)
	if !fileExists(dbFile) {
		log.Panic("DB does not exist")
	}

	return dbFile
}

func GetColumnData(dbFile, dbName string) (*sqlx.Rows, error) {
	q := sq.Select("name, type, pk, `notnull`").From(fmt.Sprintf("pragma_table_info(\"%s\")", dbName))

	sql, args, _ := q.ToSql()
	db, err := Connectx(dbFile)
	if err != nil {
		log.Panic(err)
	}

	log.Println(sql, args)
	rows, err := db.Queryx(sql, args...)
	// defer rows.Close()

	return rows, errors.Wrap(err, "DB Error")
}

// func CreateDB(dbFile string) (*sqlx.DB) {
// }

const (
	DEFAULT_COLUMNS = "rowid as _rowid, _created_at, _updated_at"
)

func Query(dbFile string, dbName string, values JSONAPIFilter, fields JSONAPIField) (*sqlx.Rows, error) {
	q := sq.Select().From(dbName)

	if len(fields) == 0 {
		q.Column("*")
	}

	for k, v := range fields {
		values := strings.Split(v[0], ",")
		// fmt.Println(k, values)
		for _, val := range values {
			q.Column(fmt.Sprintf("%s.%s", k, val))
		}
	}

	q.Columns(DEFAULT_COLUMNS)

	for k, v := range values {
		q.Where(sq.Eq{k: v[0]})
	}

	sql, args, _ := q.ToSql()
	db, err := Connectx(dbFile)
	if err != nil {
		log.Panic(err)
	}

	log.Println(sql, args)
	rows, err := db.Queryx(sql, args...)

	return rows, errors.Wrap(err, "DB Error")
}

func Insert(dbFile string, dbName string, insertValues map[string]interface{}) (sql.Result, error) {
	q := sq.Insert(dbName)

	var vs []interface{}
	for k, v := range insertValues {
		q.Columns(k)
		vs = append(vs, v)
	}

	q.Values(vs...)
	sql, args, _ := q.ToSql()

	db, err := Connectx(dbFile)
	if err != nil {
		log.Panic(err)
	}

	log.Println(sql, args)
	res, err := db.Exec(sql, args...)

	return res, err
}

func Update(dbFile string, dbName string, values JSONAPIFilter, updatedValues map[string]interface{}) {
	q := sq.Update(dbName)

	for k, v := range values {
		q.Where(sq.Eq{k: v[0]})
	}

	for k, v := range updatedValues {
		q.Set(k, v)
	}

	sql, args, _ := q.ToSql()
	db, err := Connectx(dbFile)
	if err != nil {
		log.Panic(err)
	}

	log.Println(sql, args)
	db.MustExec(sql, args...)
}

func RowsToJSON(rows *sqlx.Rows) *[]map[string]interface{} {
	totalResults := make([]map[string]interface{}, 0)

	for rows.Next() {
		results := make(map[string]interface{})
		rows.MapScan(results)

		totalResults = append(totalResults, results)
	}

	return &totalResults
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
