package server

import (
	"fmt"
	"log"
	"reflect"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	dynamicstruct "github.com/ompluscator/dynamic-struct"
	"github.com/tidwall/shardmap"
)

// type StructCache *map[string]dynamicstruct.DynamicStruct
type StructCache *shardmap.Map

func NewStructCache() StructCache {
	// _cache := make(map[string]dynamicstruct.DynamicStruct)
	_cache := shardmap.New(100)
	return _cache
}

func structFieldName(name string) string {
	words := strings.Split(name, "_")
	for index, word := range words {
		words[index] = strings.Title(word)
	}
	return strings.Join(words, "")
}

func defineTag(tagName, tagType string, omitempty bool) string {
	if omitempty {
		return fmt.Sprintf(`jsonapi:"%s,%s,omitempty"`, tagType, tagName)
	}

	return fmt.Sprintf(`jsonapi:"%s,%s"`, tagType, tagName)
}

func DefineStruct(dbName string, fields *[]map[string]interface{}) dynamicstruct.DynamicStruct {
	def := dynamicstruct.NewStruct()

	for _, field := range *fields {
		fieldName := field["name"].(string)
		column := structFieldName(fieldName)
		tagName := strings.ToLower(fieldName)

		_type := field["type"].(string)
		nullable := !convertInt64ToBool(field["notnull"].(int64))

		isPk := "attr"
		if field["pk"].(int64) == 1 {
			isPk = "primary"
		}

		config := struct {
			columnName string
			columnType interface{}
			tag        string
		}{}

		if isPk == "primary" {
			if _type == "INTEGER" {
				def.AddField(column, 0, defineTag(dbName, isPk, false))
				continue
			} else {
				log.Panicf("type %s not settable for PK", _type)
			}
		}

		if _type == "INTEGER" {
			val := 0
			valPtr := &val
			config.columnName = column
			config.tag = defineTag(tagName, "attr", false)

			if nullable {
				config.columnType = valPtr
			} else {
				config.columnType = val
			}
		} else if _type == "TEXT" {
			val := ""
			valPtr := &val
			config.columnName = column
			config.tag = defineTag(tagName, "attr", false)

			if nullable {
				config.columnType = valPtr
			} else {
				config.columnType = val
			}
		} else if _type == "TIMESTAMP" {
			val := time.Now()
			valPtr := &val
			config.columnName = column
			config.tag = defineTag(tagName, "attr", false)

			if nullable {
				config.columnType = valPtr
			} else {
				config.columnType = val
			}
		} else if _type == "DATETIME" {
			val := time.Now()
			valPtr := &val
			config.columnName = column
			config.tag = defineTag(tagName, "attr", false)

			if nullable {
				config.columnType = valPtr
			} else {
				config.columnType = val
			}
		} else {
			log.Printf("type %s not accounted for", _type)
		}

		def.AddField(config.columnName, config.columnType, config.tag)
	}

	_struct := def.Build()
	return _struct
}

func UnmarshalStruct(rows *sqlx.Rows, retResults *JSONAPIResponse, _struct dynamicstruct.DynamicStruct) {
	for rows.Next() {
		results := make(map[string]interface{})
		rows.MapScan(results)

		dynamicStruct := _struct.New()

		for field, value := range results {
			if value == nil {
				continue
			}
			field = structFieldName(field)

			t := reflect.ValueOf(dynamicStruct).Elem().FieldByName(field)
			if !t.IsValid() {
				continue
			}
			fieldValue := t.Interface()

			switch fieldValue.(type) {
			case int:
				t.SetInt(value.(int64))
			case *int:
				if value != nil {
					valPtr := value.(int)
					t.Set(reflect.ValueOf(&valPtr))
				}

			case string:
				t.SetString(value.(string))
			case *string:
				if value != nil {
					valPtr := value.(string)
					t.Set(reflect.ValueOf(&valPtr))
				}

			case time.Time:
				if value != nil {
					t.Set(reflect.ValueOf(value))
				}
			case *time.Time:
				if value != nil {
					valPtr := value.(time.Time)
					t.Set(reflect.ValueOf(&valPtr))
				}
			}
		}

		*retResults = append(*retResults, dynamicStruct)
	}
}

func GetStruct(dbFile, dbName string) dynamicstruct.DynamicStruct {
	cacheKey := fmt.Sprintf("%s:%s", dbFile, dbName)

	if val, ok := (*cache).Get(cacheKey); ok {
		log.Printf("cache struct hit: %s\n", cacheKey)
		return val.(dynamicstruct.DynamicStruct)
	} else {
		log.Printf("cache struct miss: %s\n", cacheKey)

		// build the dynamic struct
		rows, _ := GetColumnData(dbFile, dbName)
		defer rows.Close()
		fields := RowsToJSON(rows)
		_struct := DefineStruct(dbName, fields)
		// // m.Lock()
		// (*cache)[cacheKey] = _struct
		(*cache).Set(cacheKey, _struct)
		// m.Unlock()

		return _struct
	}
}

func convertInt64ToBool(val int64) bool {
	if val == 1 {
		return true
	}

	return false
}
