package server

import (
	"github.com/gofiber/fiber"
	ja "github.com/google/jsonapi"
)

type JSONAPI struct {
	// Page  map[string][]string `form:"page"`
	Field  JSONAPIField  `form:"fields"`
	Filter JSONAPIFilter `form:"filter"`
	// Sort  string              `form:"sort"`
	// Name  Name                `form:"name"`
}

// type JSONAPIArray struct {
// 	Values []string `form`
// }

type JSONAPIField map[string][]string
type JSONAPIFilter map[string][]string

type JSONAPIResponse []interface{}

func WriteJSONApiResponse(c *fiber.Ctx, results *JSONAPIResponse) {
	ja.MarshalPayload(c.Fasthttp.Response.BodyWriter(), *results)
	c.Set("Content-Type", "application/json")
}
