package cmd

import (
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/silasb/superglue/tokens"
)

var (
	exp  string
	expD time.Duration

	claimsFlag []string
	claims     map[string]interface{}
)

func init() {
	tokensCmd.Flags().StringVarP(&exp, "exp", "e", "24h", "config file (default is $HOME/.cobra.yaml)")

	tokensCmd.Flags().StringArrayVarP(&claimsFlag, "claims", "c", []string{}, "")

	rootCmd.AddCommand(tokensCmd)
}

var tokensCmd = &cobra.Command{
	Use:   "tokens:generate",
	Short: "Generate tokens",
	Long:  `Generate tokens`,
	PreRun: func(cmd *cobra.Command, args []string) {
		var e int
		var d rune

		fmt.Sscanf(exp, "%d%c", &e, &d)

		switch d {
		case 'd':
			expD = time.Hour * 24 * time.Duration(e)
		case 'h':
			expD = time.Hour * time.Duration(e)
		default:
			fmt.Println("invalidate duration")
			os.Exit(1)
		}

		claims = make(map[string]interface{})

		claimParams, _ := url.ParseQuery(strings.Join(claimsFlag, "&"))

		for claimKey, claimValues := range claimParams {
			if len(claimValues) == 1 {
				claims[claimKey] = claimValues[0]
			} else {
				claims[claimKey] = claimValues
			}
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("generate token")
		token, err := tokens.Generate(&expD, &claims)
		if err != nil {
			panic(err)
		}
		fmt.Println(token)
	},
}
