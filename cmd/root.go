package cmd

import (
	"fmt"
	"os"

	"gitlab.com/silasb/superglue/server"

	"github.com/spf13/cobra"
)

// Execute executes the root command.
var rootCmd = &cobra.Command{
	Use:   "",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		server.Start()
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
