package tokens

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	DEFAULT_TTL = time.Second * 30
)

var (
	DEFAULT_CLAIMS = map[string]interface{}{
		"otp": true,
	}
)

func Generate(expD *time.Duration, claims *map[string]interface{}) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	if expD == nil {
		t := DEFAULT_TTL
		expD = &t
	}

	if claims == nil {
		claims = &DEFAULT_CLAIMS
	}

	// Set claims
	jwtClaims := token.Claims.(jwt.MapClaims)
	for k, v := range *claims {
		jwtClaims[k] = v
	}

	jwtClaims["exp"] = time.Now().Add(*expD).Unix()

	jwtString, err := token.SignedString([]byte("secret"))
	return jwtString, err
}
