# Superglue

> A generic JSON:API interface to sqlite3

This is an JSON:API API that talks to sqlite3.  This uses Golang reflections to make things dynamic at runtime.

## ⚙️ Installation

First of all, [download](https://golang.org/dl/) and install Go. `1.11` or higher is required.

Installation is done using the [`go install`](https://golang.org/cmd/go/#hdr-Add_dependencies_to_current_module_and_install_them) command:

```bash
go install gitlab.com/silasb/superglue
```

```
superglue
```

## 🎯 Features

- Dynamic interface to create/read/update sqlite3 databases
- JSON:API interface (response schema)
- OTP tokens using JWTs

TODO

- JSON:API request schema
- Pagination
- Sorting
- Includes
- Better configuration

## 💡 Philosophy

I wanted something as easy as AirTable but didn't like how the auth interface worked. I ended up creating this with the intention to use it for my Wedding invitation site.

## 👀 Examples

This uses JWT One time use tokens.  These helpers will make the existing examples easier to read.

```bash
token() { http POST localhost:3000/token | jq -r .token; }
mhttp() { http "$@" "Authorization:Bearer $(token)"; }
```

### Creating a database

```bash
mhttp --form POST localhost:3000/csv db@sample1.csv name=test
```

The headers will become columns of the sqlte3 database named `test`.

You'll get `id`, `created_at`, and `updated_at` columns added as well.  `id` is the only column that has an index on it.

The table has a trigger that will update the `updated_at` when ever the data changes.

### Insert data

```bash
mhttp localhost:3000/test/insert name=silas address=cool
```

### Query data

Get back all records
```bash
mhttp localhost:3000/test/query
```

Filter on single column
```bash
mhttp localhost:3000/test/query filter[name]==silas
```

Filter on multiple columns
```bash
mhttp localhost:3000/test/query filter[name]==silas filter[address]==cool
```

Returning only a subset of fields
```bash
mhttp localhost:3000/test/query filter[name]==silas fields[test]==address
```

## Development

```bash
fd '.go' | entr -r -c go run main.go queries.go csv.go structs.go openapi.go
```

## ⚠️ License

Copyright (c) 2020-present [SilasB](https://gitlab.com/silasb) and [Contributors](https://gitlab.com/silasb/superglue/-/graphs/masterg). `Superglue` is free and open-source software licensed under the [MIT License](https://gitlab.com/silasb/superglue//LICENSE).
