# Superglue

> A generic JSON:API interface to sqlite3

This is an JSON:API API that talks to sqlite3.  This uses Golang reflections to make things dynamic at runtime.


# ⚙️ Installation

## Getting started

First of all, [download](https://golang.org/dl/) and install Go. `1.11` or higher is required.

Installation is done using the [`go install`](https://golang.org/cmd/go/#hdr-Add_dependencies_to_current_module_and_install_them) command:

```bash
go install gitlab.com/silasb/superglue
```

## Commands

* `superglue [dir-name]` - Run server with working directory at [dir-name].

## Examples

This uses JWT One time use tokens.  These helpers will make the existing examples easier to read.

```bash
token() { http POST localhost:3000/token | jq -r .token; }
mhttp() { http "$@" "Authorization:Bearer $(token)"; }
```

### Creating a database

```bash
mhttp --form POST localhost:3000/csv db@sample1.csv name=test
```

!!! note
    Running this over and over again is not supported because the trigger tries to get created again

The headers will become columns of the sqlte3 database named `test`.

You'll get `id`, `created_at`, and `updated_at` columns added as well.  `id` is the only column that has an index on it.

The table has a trigger that will update the `updated_at` when ever the data changes.

### Insert data

```bash
mhttp localhost:3000/test/insert name=silas address=cool
```

!!! note
    JSON:API body requests are not yet supported.

### Query data

Get back all records
```bash
mhttp localhost:3000/test/query
```

!!! note
    Including multiple tables is not supported

Filter on single column
```bash
mhttp localhost:3000/test/query filter[name]==silas
```

Filter on multiple columns
```bash
mhttp localhost:3000/test/query filter[name]==silas filter[address]==cool
```

Returning only a subset of fields
```bash
mhttp localhost:3000/test/query filter[name]==silas fields[test]==address
```

