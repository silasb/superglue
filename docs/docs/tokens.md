# Tokens

`/tokens` endpoint will return a JWT token that is of one time use.

Request

```bash
http localhost:3000/tokens
```

Response

```
{ "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1ODgwMzk0NDUsIm90cCI6dHJ1ZX0.EXWWCt9OCZP_3N3z8S4hNTGOzIrSXviRQgqUB2JQeB8" }
```

Where the JWT is defined as: [jwt.io](https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1ODgwMzk0NDUsIm90cCI6dHJ1ZX0.EXWWCt9OCZP_3N3z8S4hNTGOzIrSXviRQgqUB2JQeB8)

!!! note
    More documentation around this is needed.