package main

import (
	"gitlab.com/silasb/superglue/cmd"
)

func main() {
	cmd.Execute()
}
