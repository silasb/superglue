module gitlab.com/silasb/superglue

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/elgris/sqrl v0.0.0-20190909141434-5a439265eeec
	github.com/go-chi/cors v1.1.1 // indirect
	github.com/go-playground/form v3.1.4+incompatible // indirect
	github.com/go-playground/form/v4 v4.1.1
	github.com/gofiber/cors v0.0.3
	github.com/gofiber/fiber v1.9.2
	github.com/gofiber/jwt v0.0.6
	github.com/gofiber/recover v0.0.3
	github.com/google/jsonapi v0.0.0-20200226002910-c8283f632fb7
	github.com/huandu/go-sqlbuilder v1.7.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/klauspost/compress v1.10.5 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/mitchellh/go-homedir v1.1.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/ompluscator/dynamic-struct v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/tidwall/lotsa v1.0.1 // indirect
	github.com/tidwall/rhh v1.1.0 // indirect
	github.com/tidwall/shardmap v0.0.0-20190927132224-c190691bd211
	github.com/valyala/fasttemplate v1.1.0
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
